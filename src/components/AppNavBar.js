// Long method
// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';

// Short method
import { useContext } from 'react';
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar(){

	//A context object such as our UserContext can be "opened" with React's useContext hook
	const { user } = useContext(UserContext);

	return(
		<Navbar bg="light" expand="lg">
		  <Container>
		    <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
			{/*
				- className is use instead class, to specify a CSS class

				- changes for Bootstrap 5
				from mr -> to me
				from ml -> to ms
			*/}
		      <Nav className="ms-auto">
		        <Nav.Link as={NavLink} to="/">Home</Nav.Link>
		        <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>

		        {(user.email !== null) ?
		        	<Link className='nav-link' to="/logout">Logout</Link>

		        	:
		        	<>
		        	<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
		        	<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
		        	</>
		        } 

		        
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
	)
}