import { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout() {
	const { setUser } = useContext(UserContext);

	localStorage.clear();

	useEffect(() => {
		setUser({
			email: null
		})
	})

	return(
		// redirects the user to login page
		<Redirect to="/login"/>
	)
}

// Homework Activity: Add a functionality to redirect the user back to the homepage if they are logged in and try to access the login or register pages