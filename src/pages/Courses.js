import { useEffect, useState } from 'react';
// import courseData from '../data/courseData';
import CourseCard from '../components/CourseCard';

export default function Courses(){

	const [coursesData, setCourses] = useState([]);

	// Check to see if the mock data wa captured
	// console.log(courseData);
	// console.log(courseData[0]);

	// Props
		// is a shorthand for "property" since components are considered as object in ReactJS.
		// Props is a way to pass data from a parent component to a child component.
		// It is synonymous to the function parameter.
		// This is reffered to as "props drilling".

	// Fetch by default always makes a GET request, unless a different one is specified
	// ALWAYS add fetch request for getting data in a useEffect hook

	useEffect(() => {
		// console.log(process.env.REACT_APP_API_URL)
		// changes to env files are applied ONLY at build time (when starting the project locally)

		fetch(`${process.env.REACT_APP_API_URL}/courses`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCourses(data)
		})
	}, [])


	const courses = coursesData.map(course =>{
		return(
			<CourseCard courseProp={course} key={course._id} />
		)
	})

	return(
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}